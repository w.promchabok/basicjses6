//===ตัวอย่างชนิดข้อมูลใน JavaScript (Premitive Type)
console.log(typeof 23); //number
console.log(typeof "23") //string
console.log(typeof 23.50); //number
console.log(typeof false); //boolean
console.log(typeof true); // boolean
console.log(typeof undefined); // undefined
console.log(typeof null); // object

//Compare
console.log(null === undefined); //false

//===ค่าเริ่มต้นของ variable เป็น undefined
var x;
console.log(x); //undefined

//===ค่าเริ่มต้นของฟังก์ชั่น เป็น undefined เช่นกัน
function a(){}
console.log(a()) //undefined

var y = null 
console.log(y); //null


function b(){ return null}
console.log(b()); //null


//===ชนิดข้อมูลแบบ Object (ไม่ใช่ Premitive Type)
//Array, Objects, Function, Class Instance
var foo = Boolean(false);
var boo = String('Hello');
console.log(typeof Boolean) //function
console.log(typeof String) //function
