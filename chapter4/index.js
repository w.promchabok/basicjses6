//==== การสร้างตัวแปร ใน JavaScript

// x = 'smile';
// var x = 'smile';
// let x = 'smile';
// const x = 'smile'
// console.log(x)

//========ตัวแปรแบบ var สร้างขึ้นมาแล้วกำหนดค่าใหม่เข้าไปได้เรื่อยๆ
var x ;
x = 'smile';
x = 'something';
x = 'something else'; 
console.log(x) //something else

//========= ตัวแปรแบบ var ไม่สนใจ scope ถูกมองเป็น Global ==========

var a = 10;
{
    var a = 20;
}

console.log(a); //20
//============

var g = 'global';
function app() {
    var g = 'local';
    console.log(g) //local 
}
app(); // No result เพราะ งงว่าจะเอาค่าไหนมาโชว์


//========ตัวแปรแบบ let สนใจ scope กำหนดค่าใหม่เข้าไปได้เรื่อยๆ  =================

let b = 10;
{
    let b = 20;
}

console.log(b); //10
//===================
var g = 'global';
function applet() {
    var l = 'local';
    console.log(l); // local
}
applet(); 


//========ตัวแปรแบบ const สนใจ scope ไม่สามารถกำหนดค่าใหม่เข้าไปได้อีก
if (true) {
    const xd = 23;
    xd = 25; // Assignment to constant variable. 
}
console.log(xd);
