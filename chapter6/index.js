//====== การใช้ Object ใน JavaScript

// พื้นฐาน Object
// Array
const arr = [1, 2, 3, 4, 5];
console.log(arr) // [1, 2, 3, 4, 5]


//Object
// KEY : VALUE
//แบบดั้งเดิม
const obj1 =  new Object();
obj1.name = 'Szeged';
obj1.color = 'Blue';
console.log(obj1); // { name: 'Szeged', color: 'Blue' }

//========== แบบใหม่ สั้นๆ
const obj2 = {
    name:'Szeged',
    color: 'Blue',
    age: 29,
    status : false
}

console.log(obj2); // { name: 'Szeged', color: 'Blue', age: 29, status : false }



//========== การเปลี่ยนแปลงค่าภายใน object เราเรียกว่าการ Mutate
obj2.name = 'Johny'
console.log(obj2); //{ name: 'Johny', color: 'Blue', age: 29, status : false }


//======= Object สามารถเก็บ value เป็นฟังก์ชั่นได้

const obj3 = {
    name:'Szeged',
    color: 'Pink',
    age: 15,
    hello : function(){
        return `hello ${this.name}`;
    }
}

console.log(obj3); //{ name: 'Szeged', color: 'Pink', age: 15, hello : [λ: hello] }
console.log(obj3.hello()); //hello Szeged

//===== มาดูการทำงานของ this

const obj4 = {
    name:'Szeged',
    color: 'Pink',
    age: 15,
    hello1 : function(){
        console.log(this); // {name: 'Szeged', color: 'Pink', age: 15, hello1 : [λ: hello1], hello2 : [λ: hello2] }
        return `hello ${this.name}`;
    },
    hello2 : () => {
        console.log(this); //{}
    }
}
obj4.hello1();
obj4.hello2();

 