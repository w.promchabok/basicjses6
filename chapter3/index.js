//====ใน JavaScript เรานิยมใช้ if...else statement กำหนดเงื่อนไขแบบพื้นฐานเช่น

var truthy = false; //true //false //null

if(truthy){
    //do this var truthy = true;
    console.log("Hello");
}else if(truthy == null){
    //do this var truthy = null;
    console.log("Good bye");
}else{
    //do this var truthy = false;
    console.log("Bye Bye");
}

//=====ความเข้าใจเกี่ยวกับ True/ False


//=====true
console.log(true); //true
console.log(! {}) //false //{}คือ object
console.log(!! {}) //true //{}คือ object
console.log(!! []) //true //[]คือ array


//=====false
console.log(false); //false
console.log(! {}) //false //{}คือ object
console.log(!! 0) //false //{}คือ object
console.log(! -1) //false //[]คือ array

//===== Operator in JavaScript
var x = true;
console.log(x); //true
console.log(!x); //false

var a = true && false; 
console.log(a) //false


var val = "23" == 23  // ถ้าใช้ = 2ตัว จะไม่สนใจ type
console.log(val); //true

var val = "23" === 23 // ถ้าใช้ = 3ตัว จะสนใจ type ***Recommended***
console.log(val); //false


//====Ternary Operator

//เขียนปกติ
var truthy2 = false;
var x;

if (truthy2) { //true
    x = 1;
} else { //false
    x = 2;
}
console.log(x) // 2

//เขียนแบบ Ternary
var truthy3 = false; 
var x2 = truthy2 ? 1 : 2; //Ternary Operator
console.log(x2) //2