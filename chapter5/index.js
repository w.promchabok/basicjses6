//===Function in JavaScript


//create function
function hello(input) {
    const output = 'Hi ' + input + ' nice to meet you.';
    return output
}

//call
console.log(hello('Szeged')); //Hi Szeged nice to meet you.


//====Arrow Function ใน ES 6

//const hello = (input) => output;
//const hello = input => output; มี paramitor ตัวเดียวไม่จำเป็นต้องมีวงเล็บ

const hello2 = input => 'Hi ' + input + ' nice to meet you.';
console.log(hello2('Szeged')); //Hi Szeged nice to meet you.

